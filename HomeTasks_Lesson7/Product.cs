﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeTasks_Lesson7
{
    enum ProductTypes
    {
        Confectionery,
        Bake,
        Vegetables,
        Fruits,
        Groats,
        Conserve
    }

    struct Product
    {
        private int id;
        private string name;
        private ProductTypes type;
        private string description;
        private float price;

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                if (value != 0)
                    id = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != null)
                    name = value;
            }
        }

        public float Price
        {
            get
            {
                return price;
            }
            set
            {
                if (value != 0)
                    price = value;
            }
        }

        public string Descripton
        {
            get
            {
                return description;
            }
            set
            {
                if(value != null)
                    description = value;
            }
        }

        public Product(int id, string name, ProductTypes type, string description, float price)
        {
            this.id = id;
            this.name = name;
            this.type = type;
            this.description = description;
            this.price = price;
        }
    }
}
