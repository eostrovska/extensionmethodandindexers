﻿using System;

namespace HomeTasks_Lesson7
{
    class Stotre
    {
        private Product[] products = new Product[]{
            new Product(134, "Marshmallows", ProductTypes.Confectionery, "Sweet marshmallows with a lot of sugar. Manufacture:Roshen", 45.9F),
            new Product(267, "Dark chocolate", ProductTypes.Confectionery, "Italian chocolate. Manufacture:Roshen", 45.5F),
            new Product(673, "Halva", ProductTypes.Confectionery, "Made of seeds. Manufacturer:Sonyashnik.", 13.34F),
            new Product(489, "Baguette", ProductTypes.Bake, "Flour - the highest grade.", 10),
            new Product(34890, "Beet", ProductTypes.Vegetables, "Grown in Ukraine", 4.55F),
            new Product(0945, "Potato", ProductTypes.Vegetables, "Grown in Ukraine.", 5.67F),
            new Product(3467, "Banana", ProductTypes.Fruits, "From Turkey.", 23.99F),
            new Product(45098, "Buckwheat", ProductTypes.Groats, "Packed in 900 grams.", 34.34F),
            new Product(2380, "Tuna", ProductTypes.Conserve, "Tuna in its own juice.", 56.60F)
            };

        public string this[int index]
        {
            get
            {
                for (int i = 0; i < products.Length; i++)
                {
                    if (products[i].Id == index)
                    {
                        return products[i].Descripton;
                    }
                }
                return null;
            }
        }

        public string this[string index]
        {
            get
            {

                for (int i = 0; i < products.Length; i++)
                {
                    if (products[i].Name == index)
                    {
                        return products[i].Descripton;
                    }
                }

                Console.WriteLine("This product does not exist!");
                return null;
            }
        }
    }
}
