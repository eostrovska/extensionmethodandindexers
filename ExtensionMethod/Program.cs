﻿using System;

namespace ExtensionMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Task 2: Create an extension method for an integer array that sorts the elements of an array by ascending");

            int[] array = new int[] { 34, 56, 7, 6, 12, 9, 80, 76, 45, 2, 56, 89, 54, 3, 76, 21 };
            array.CustomSortAsk();

            Console.ReadKey();
        }
    }
}
